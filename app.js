const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '',
      confirmedName: '',
    };
  },
  methods: {
    add(num) {
      this.counter = this.counter + num;
    },
    remove(num) {
      this.counter = this.counter - num;
    },
    setName(event) {
      this.name = event.target.value;
    },
    submitForm() {
      alert('Submitted !');
    },
    resetInput() {
      this.confirmedName = '';
    }
  },
  computed: {
    confirmedName() {
      this.confirmedName = this.name;
    }
  },
  watch: {
    counter(value) {
if(value > 50){
  this.counter = 0;
}
    }
  }
});

app.mount('#events');
